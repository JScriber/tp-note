package tp;

public class Main {
    public static void main(String[] args) {
        Article article = new Article("Chaussettes", 10, 5.5);

        // Ex 1.
        System.out.println("Exercice 1");
        System.out.println("Designation: " + article.designation());
        System.out.println("Quantité: " + article.quantite());
        System.out.println("Prix HT total: " + article.prixHt());
        System.out.println("Prix TTC total: " + article.prixTtc());

        System.out.println("---------------------");

        // Ex 2.
        // Comment qualifie-t-on le lien entre cette nouvelle déclaration de la
        // méthode toString() et celle figurant dans la classe Object ?
        // Réponse: Une surcharge.
        System.out.println("Exercice 2");
        System.out.println(article);

        System.out.println("---------------------");

        // Ex 3 et 4.
        Article livre = new Livre("The Book", 25, 22.90, 198, "2-7475-4567");
        System.out.println("Exercice 3 et 4");
        System.out.println(livre);

        System.out.println("---------------------");

        // Ex 5.
        Cd cd = new Cd("The CD", 60, 30.0, "Pop", "Bellanger Jérémie");
        System.out.println("Exercice 5");
        System.out.println(cd);
    }
}
