package tp;

public class Cd extends Article {

    private String rubrique;

    private String auteur;

    public Cd(String designation, int quantite, double prixHt, String rubrique, String auteur) {
        super(designation, quantite, prixHt);
        this.rubrique = rubrique;
        this.auteur = auteur;
    }

    @Override
    public String toString() {
        return super.toString() + ", Rubrique : " + this.rubrique + ", Auteur : " + this.auteur;
    }
}
