package tp;

import java.util.ArrayList;
import java.util.List;

public class CatalogueCollection {

    List<Article> articles;

    public CatalogueCollection() {
        this.articles = new ArrayList<>();
    }

    public void ajouter(Article article) {
        articles.add(article);
    }

    public void afficher() {
        for (Article article : articles) {
            System.out.println(article);
        }
    }
}
