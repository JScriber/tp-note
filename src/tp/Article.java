package tp;

public class Article implements IArticle {

    private static final double TVA = 1.2;

    private String designation;

    private int quantite;

    private double prixHt;

    public Article(String designation, int quantite, double prixHt) {
        this.designation = designation;
        this.quantite = quantite;
        this.prixHt = prixHt;
    }

    @Override
    public String designation() {
        return this.designation;
    }

    @Override
    public int quantite() {
        return this.quantite;
    }

    @Override
    public double prixHt() {
        return this.prixHt;
    }

    @Override
    public double prixTtc() {
        return this.prixHt * TVA;
    }

    @Override
    public void ajouter(int quantite) {
        this.quantite += quantite;
    }

    @Override
    public void retirer(int quantite) {
        this.quantite -= quantite;
    }

    @Override
    public String toString() {
        return designation + ", " + prixHt + " euros (" + quantite + " en stock)";
    }
}
