package tp;

public interface ILivre extends IArticle {

    int nombrePages();

    String numeroISBN();
}
