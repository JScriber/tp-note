package tp;

public interface IArticle {

    String designation();
    int quantite();
    double prixHt();
    double prixTtc();

    void ajouter(int quantite);
    void retirer (int quantite);

}
