package tp;

import tp.utils.Saisie;

import java.util.Scanner;

public class TestCatalogueCollection {

    public static void main(String[] args) {
        // Exercice 9.
        try (Scanner scanner = new Scanner(System.in)) {
            CatalogueCollection catalogue = new CatalogueCollection();
            boolean continuer = true;
            int totalArticle = 0;

            while (continuer) {
                int articleType = -1;
                String menu = "Quel est l'article n°" + (totalArticle + 1) + " voulu:\n" +
                        "1) CD\n" +
                        "2) Livre\n" +
                        "3) Arrêter";

                while (articleType < 1 || articleType > 3) {
                    System.out.println("-----------------------------------------");
                    articleType = Saisie.nombre(scanner, menu);
                }

                if (articleType == 3) {
                    continuer = false;
                } else {
                    String designation;
                    int quantite = 0;
                    double prixHt = -1;

                    designation = Saisie.texte(scanner, "Désignation ?");

                    while (quantite <= 0) {
                        quantite = Saisie.nombre(scanner, "Quantité ?");
                    }

                    while (prixHt < 0) {
                        prixHt = Saisie.nombreDecimal(scanner, "Prix HT ?", -1);
                    }

                    switch (articleType) {
                        case 1:
                            String rubrique = Saisie.texte(scanner, "Rubrique ?");
                            String auteur = Saisie.texte(scanner, "Auteur ?");

                            catalogue.ajouter(new Cd(designation, quantite, prixHt, rubrique, auteur));
                            totalArticle++;
                            break;
                        case 2:
                            int pages = 0;

                            while (pages < 1) {
                                pages = Saisie.nombre(scanner, "Nombre de pages ?");
                            }
                            String isbn = Saisie.texte(scanner, "Numéro ISBN ?");

                            catalogue.ajouter(new Livre(designation, quantite, prixHt, pages, isbn));
                            totalArticle++;
                            break;
                        default:
                            break;
                    }
                }
            }

            System.out.println("Résumé:");
            catalogue.afficher();
        }
    }
}
