package tp;

public class Livre extends Article implements ILivre {

    private int nombrePages;

    private String numeroISBN;

    public Livre(String designation, int quantite, double prixHt, int nombrePages, String numeroISBN) {
        super(designation, quantite, prixHt);
        this.nombrePages = nombrePages;
        this.numeroISBN = numeroISBN;
    }

    @Override
    public int nombrePages() {
        return this.nombrePages;
    }

    @Override
    public String numeroISBN() {
        return this.numeroISBN;
    }

    @Override
    public String toString() {
        return super.toString() + ", ISBN : " + this.numeroISBN + " (" + this.nombrePages + " pages)";
    }
}
