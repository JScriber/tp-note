package tp.utils;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Saisie {

    public static String texte(Scanner scanner, String question) {
        System.out.println(question);
        return scanner.next();
    }

    public static int nombre(Scanner scanner, String question) {
        System.out.println(question);
        int nombre;

        try {
            nombre = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Entrée non-reconnue.");
            nombre = 0;
        }

        scanner.nextLine();

        return nombre;
    }

    public static double nombreDecimal(Scanner scanner, String question) {
        return nombreDecimal(scanner, question, 0);
    }

    public static double nombreDecimal(Scanner scanner, String question, double valeurDefaut) {
        System.out.println(question);
        double nombre;

        try {
            nombre = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Entrée non-reconnue.");
            nombre = valeurDefaut;
        }

        scanner.nextLine();

        return nombre;
    }
}
