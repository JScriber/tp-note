package tp;

public class Catalogue {

    Article[] articles;

    int nombreArticles;

    int quantiteMax;

    public Catalogue(int quantiteMax) {
        this.articles = new Article[quantiteMax];
        this.nombreArticles = 0;
        this.quantiteMax = quantiteMax;
    }

    public void ajouter(Article article) throws Exception {
        if (nombreArticles == quantiteMax) {
            throw new Exception("Quantité max atteinte.");
        }

        articles[nombreArticles] = article;
        nombreArticles++;
    }

    public void afficher() {
        for (Article article : articles) {
            if (article != null) {
                System.out.println(article);
            }
        }
    }
}
